module.exports = {
  branch: require( './branch' ),
  checkout: require( './checkout' ),
  commit: require( './commit' ),
  push: require( './push' ),
};
